Hello, **#pivorak** team is calling you!

Huston, we have a problem and only you, the brave programming learner can help us!

[Wunderlist](https://www.wunderlist.com/) -– the best todo-list application ever was bought by Microsoft and now became a Microsoft To-Do. It was a shock for us, cause we loved Wunderlist so much and can't imagine our lives without it. Long story short, we've decided to develop our own todo-app. And we are asking you for help.

Your goal is to create a prototype of Wunderlist, but on a website.

The **main requirements** are pretty basic: User of your todo website should be able to:
- create list of todo's
- see the list of todo's
- edit list title
- add a new todo to the list
- mark todo as complete
- mark todo as incomplete
- edit todo
- delete todo from the list

In your implementation you should:
- use Ruby on Rails to build the project
- publish project to GitLab
- submit link to the project through [pivorak website](https://pivorak.com/courses/seasons/pivorak-ruby-summer-course-2020) until **April 17, 2020**

Looks pretty simple, right?

So, if you are done with the main part you can improve your prototype and give it more functionality!

Next steps are NOT obligatory and might be done only when the main requirements are ready to go.

As an option you can add **extra functions**:
- User can register on website, login and logout
- User can create lists that only registered user can see
- All good code should be covered with tests. We bet you can achieve 90% of test coverage (it can be easily measured with SimpleCov tool).

Don't hesitate to submit the not-completed task, as every effort counts and we will review your work in any case!

**Have a great time coding and may the force be with you!**
