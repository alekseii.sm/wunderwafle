# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TodoList do
  it { should validate_presence_of(:title) }
  it { should have_many(:todos) }
end
