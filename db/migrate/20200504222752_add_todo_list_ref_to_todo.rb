class AddTodoListRefToTodo < ActiveRecord::Migration[5.2]
  def change
    add_reference :todos, :todo_list, foreign_key: true
  end
end
