class CreateTodoLists < ActiveRecord::Migration[5.2]
  def change
    create_table :todo_lists do |t|
      t.references :todo, foreign_key: true
      t.string :title

      t.timestamps
    end
  end
end
