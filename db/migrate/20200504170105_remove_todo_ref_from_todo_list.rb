class RemoveTodoRefFromTodoList < ActiveRecord::Migration[5.2]
  def change
    remove_column :todo_lists, :todo_id, :integer
  end
end
