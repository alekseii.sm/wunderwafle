# frozen_string_literal: true

Rails.application.routes.draw do
  root 'todo_lists#index'
  resources :todo_lists do
    resources :todos
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
