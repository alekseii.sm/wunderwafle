# frozen_string_literal: true

class Todo < ApplicationRecord
  belongs_to :todo_list

  validates_presence_of :description
end
