# frozen_string_literal: true

class TodoList < ApplicationRecord
  validates_presence_of :title
  has_many :todos, dependent: :destroy
end
